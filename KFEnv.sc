KFEnv{

    var <>levels;
    var <>times;
    var <>pollingRate;
    var <value;
    var <routine;
    var <env;

    *new {
        arg levels=#[0, 1, 0], times=#[1, 1], pollingRate=1;
        ^super.newCopyArgs(levels, times, pollingRate).init;
    }

    init { 
        this.prCreateRoutine;
    } 

    play {
        routine.play;
    }

    prCreateRoutine { 
        routine = Routine{
            env = Env(levels, times).asStream;
            inf.do{
                if(levels.next == levels[levels.size-1]){
                    this.stop;
                };
                value = env.next;
                pollingRate.wait;
            }
        }
    }

    stop {
        routine.stop;
    }
} 
