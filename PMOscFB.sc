PMOscFB  {

	*ar { arg carfreq,modfreq,pmindex=0.0,modFeedback=0.0,mul=1.0,add=0.0;
		^SinOscFB.ar(carfreq, SinOscFB.ar(modfreq, modFeedback, pmindex),mul,add)
	}

	*kr { arg carfreq,modfreq,pmindex=0.0,modFeedback=0.0,mul=1.0,add=0.0;
		^SinOscFB.kr(carfreq, SinOscFB.kr(modfreq, modFeedback, pmindex),mul,add)
	}

}
