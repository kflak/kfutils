MakeSynthDef {
    var <name;
    var <func;
    var <envFunc;
    var <pan;
    var <numOutputChannels;
    var panFunc;
    var envelope;

    *new { |name=\mySynth, func, env=\asr, pan=true, numOutputChannels=2|
        ^super.newCopyArgs(name, func, env, pan, numOutputChannels).init;
    }

    init {
        envelope = (
            asr: { 
                EnvGen.kr(Env.asr( 
                    \attack.kr(0.01), 
                    1, 
                    \release.kr(0.01)
                ), 
                gate: \gate.kr(1), 
                doneAction: \da.kr(2));
            },
            adsr: {
                EnvGen.kr(Env.adsr( 
                    \attack.kr(0.01), 
                    \decay.kr(0.3),
                    \sustainLevel.kr(0.5), 
                    \release.kr(0.01)
                ), 
                gate: \gate.kr(1), 
                doneAction: \da.kr(2));

            },
            perc: {
                EnvGen.kr(Env.perc( 
                    \attack.kr(0.01),
                    \release.kr(0.01)
                ), 
                gate: 1, 
                doneAction: \da.kr(2));
            }
        );
        this.setPanFunc;
        SynthDef(name, {
            var env, sig;
            sig = SynthDef.wrap(func);
            env = SynthDef.wrap(envelope[envFunc]);
            sig = sig * \amp.kr(1) * env;
            sig = SynthDef.wrap(panFunc, prependArgs: [sig]);
            Out.ar(\out.kr(0), sig);
        }).add;
    }

    setPanFunc {
        if (pan){
            if(numOutputChannels > 2){
                panFunc = {|sig|
                    sig = PanAz.ar(
                        numOutputChannels, 
                        sig,
                        \pan.kr(0), 
                        1,
                        \width.kr(2)
                    );
                }
            };
            if(numOutputChannels == 2){
                panFunc = {|sig|
                    sig = Pan2.ar(
                        sig,
                        \pan.kr(0)
                    );
                }
            }
        }{
            panFunc = {|sig|
                sig = sig;
            }
        }
    }
}
