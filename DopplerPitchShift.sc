DopplerPitchShift {
    *ar {
        arg in, ratio=1, numDelays=8;
        var sig, phasor, buf, hanning, window, phasorFreq, phase, iphase, delayWindow=0.1;
        hanning = Signal.hanningWindow(512).asWavetable;
        buf = LocalBuf.newFrom(hanning);
        phasorFreq = ((ratio - 1 ) * -1) / delayWindow;
        window = numDelays.collect{|i|
            phase = i.linlin(0, numDelays+1, 0, 2pi);
            Osc.kr(buf, phasorFreq, phase) ;
        };
        phasor = numDelays.collect {|i|
            iphase = i.linlin(0, numDelays+1, 1, 3).mod(2);
            LFSaw.ar(phasorFreq, iphase).range(0, 1) * delayWindow;
        };
        sig = numDelays.collect{|i| DelayC.ar(in, 0.4, delaytime: phasor[i]) * window[i]};
        sig = sig.sum;
        ^sig;
    }
}
