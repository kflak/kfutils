+ SoundFile {
    *collectIntoBuffersMono { | path = "sounds/*", server |
        server = server ?? { Server.default };
        if (server.serverRunning) {
            ^this.collect(path)
            .collect { | sf |
                Buffer(server, sf.numFrames, 1)
                .allocReadChannel(sf.path, channels: [0])
                .sampleRate_(sf.sampleRate);
            }
        } {
            "the server must be running to collect soundfiles into buffers".error
        }
    }
}
